import os
import librosa as lr
import librosa.display
import matplotlib
import pylab
import numpy as np

input_folder = '/Users/harm/desktop/ExpressiveCloud_mockup/input'
output_folder = '/Users/harm/desktop/ExpressiveCloud_mockup/output'
input_files = os.listdir(input_folder)
index = 0

for file in input_files:
    index += 1
    file_path = input_folder + "/" + file
    print("Analysing file: ")
    print(file_path)

    # loads example from lr library
    y, sr = lr.load(file_path)
    # no axis
    pylab.axis('off')
    # removes white space
    pylab.axes([0., 0., 1., 1.], frameon=False, xticks=[], yticks=[])
    # gets mel features
    S = lr.feature.melspectrogram(y=y, sr=sr, n_mels=128, fmax=8000)
    # converts amplitude squared (aka power) to dB
    S_dB = lr.power_to_db(S, ref=np.max)
    # makes image
    img = librosa.display.specshow(S_dB)

    save_path = output_folder + "/" + str(index) + ".jpeg"
    print("Writing file: ")
    print(save_path)
    pylab.savefig(save_path, bbox_inches=None, pad_inches=0)
    pylab.close()
