This repository is for my mockup for my third year project about AI.


Dependencies include matplotlib, pytorch, torchaudio and librosa.

The preprocessing file will take your audiofiles and convert it to a mel spectrogram for further analysis.
